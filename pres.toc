\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Le stage}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Contexte}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Analyse et définition du problème}{7}{0}{1}
\beamer@sectionintoc {2}{Openmpi }{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Modular Component Architecture MCA}{9}{0}{2}
\beamer@subsectionintoc {2}{2}{Composant collective coll}{11}{0}{2}
\beamer@subsectionintoc {2}{4}{Bruck algorithme : alltoall}{12}{0}{2}
\beamer@sectionintoc {3}{Procédure de recette}{15}{0}{3}
\beamer@subsectionintoc {3}{1}{Mesures de référence sur PLUTON}{16}{0}{3}
\beamer@subsubsectionintoc {3}{1}{1}{Taille de message 2Ko}{17}{0}{3}
\beamer@subsubsectionintoc {3}{1}{2}{Taille de message 1Mo}{19}{0}{3}
\beamer@subsectionintoc {3}{2}{coll tuned : alltoall de Bruck}{21}{0}{3}
\beamer@subsectionintoc {3}{3}{Perspective}{23}{0}{3}
