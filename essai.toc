\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Composants mxm}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Gather}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Alltoall}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Composant ob1 et openib}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Alltoall}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Gather}{7}{subsection.3.2}
\contentsline {section}{\numberline {4}Options de binding}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}Binding et mapping}{8}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Distribution block et cyclic}{9}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}N\IeC {\oe }uds partageant le m\IeC {\^e}me switch}{11}{subsection.4.3}
\contentsline {section}{\numberline {5}Nombre de n\IeC {\oe }ud allou\IeC {\'e} fixe, distribution cyclic}{13}{section.5}
\contentsline {section}{\numberline {6}Taille de message 1Mo}{14}{section.6}
\contentsline {subsection}{\numberline {6.1}Gather}{14}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Distribution block}{14}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Distribution cyclic}{15}{subsubsection.6.1.2}
\contentsline {subsection}{\numberline {6.2}Alltoall}{16}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Distribution block}{16}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Distribution cyclic}{17}{subsubsection.6.2.2}
\contentsline {section}{\numberline {7}Conclusion}{18}{section.7}
