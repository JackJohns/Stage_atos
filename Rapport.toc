\select@language {french}
\contentsline {section}{\numberline {1}Remerciement}{1}{section.1}
\contentsline {section}{\numberline {2}Introduction}{4}{section.2}
\contentsline {section}{\numberline {3}Le Stage}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Présentation ATOS}{5}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Historique Atos}{5}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Activités}{5}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Présence dans le monde}{5}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Atos-Bull (lieu du stage)}{5}{subsubsection.3.1.4}
\contentsline {subsection}{\numberline {3.2}Contexte et objectif du stage}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Environnement de travail}{6}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Méthodologie Agile Scrum}{6}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Intel Xéon-phi KNL}{6}{subsubsection.3.3.2}
\contentsline {section}{\numberline {4}État de l'Art: Openmpi}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Arcitecture MCA d'Openmpi}{7}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Framworks OMPI et Composant COLL}{8}{subsubsection.4.1.1}
\contentsline {subsection}{\numberline {4.2}Vader BTL}{9}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Alltoall}{9}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Pairwise algorithme}{10}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Bruck algorithme}{12}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Étape 1}{12}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Étape 2}{12}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Étape 3}{12}{subsubsection.4.5.3}
\contentsline {section}{\numberline {5}Travaux effectués}{13}{section.5}
\contentsline {subsection}{\numberline {5.1}Comparaison de latence IntelMPI VS OpenMPI}{13}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Benchmarks OSU}{14}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Choix du binding avec IntelMpi}{14}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Choix du binding avec OpenMpi}{15}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}IntelMpi VS OpenMpi}{15}{subsubsection.5.1.4}
\contentsline {subsection}{\numberline {5.2}Composant collective shared d'UTK}{16}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Motivation}{17}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Implementation}{17}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}Pairwise Alltoall one sided : inta-noeuds}{17}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}Alltoall Bruck one sided : intra-noeud}{18}{subsubsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.3}Alltoall pairwise one sided : inter-noeuds }{19}{subsubsection.5.4.3}
\contentsline {subsection}{\numberline {5.5}Alltoall pairwise one seided hiérarchique}{20}{subsection.5.5}
\contentsline {subsubsection}{\numberline {5.5.1}Phase 1:}{20}{subsubsection.5.5.1}
\contentsline {subsubsection}{\numberline {5.5.2}Phase 2:}{20}{subsubsection.5.5.2}
\contentsline {subsubsection}{\numberline {5.5.3}Phase 3}{20}{subsubsection.5.5.3}
\contentsline {subsubsection}{\numberline {5.5.4}Phase 4}{21}{subsubsection.5.5.4}
\contentsline {section}{\numberline {6}Les Résultats et Validation}{21}{section.6}
\contentsline {subsection}{\numberline {6.1}Intra-noeud}{21}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Pairewise}{21}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Bruck}{21}{subsubsection.6.1.2}
\contentsline {subsection}{\numberline {6.2}Inter-noeud : Pairwise}{23}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Hiérarchique (intra et inter noeud)}{24}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Améliorations possibles}{25}{subsection.6.4}
\contentsline {section}{\numberline {7}Conclusion}{26}{section.7}
\contentsline {subsection}{\numberline {7.1}Leçons tirées}{26}{subsection.7.1}
\contentsline {section}{\numberline {A}Performance en latence shared VS tuned VS intelmpi}{27}{appendix.A}
\contentsline {subsection}{\numberline {A.1}bcast}{27}{subsection.A.1}
\contentsline {subsection}{\numberline {A.2}allreduce}{28}{subsection.A.2}
\contentsline {section}{\numberline {B}Mesure de performance en latence algorithme hierarchique (phase 3 avec MPI\_Put) VS tuned VS intelmpi}{28}{appendix.B}
