\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Influence du binding avec IntelMpi}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Default Binding}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}I\_MPI\_PIN\_DOMAINE=core}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}I\_MPI\_PIN\_DOMAINE=node}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}I\_MPI\_PIN\_DOMAINE=cache2}{4}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Gather}{4}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}alltoall}{5}{subsection.2.6}
\contentsline {section}{\numberline {3}IntelMpi VS OpenMPI}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}bcast}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Scatter/Gather}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Allgather}{10}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}reduce}{11}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}allreduce}{12}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}alltoall}{13}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}R\IeC {\'e}capitulative}{14}{subsection.3.7}
\contentsline {section}{\numberline {4}Conclusion}{14}{section.4}
\contentsline {section}{\numberline {5}Annexe}{14}{section.5}
